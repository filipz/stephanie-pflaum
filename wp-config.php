<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'intervall_stephaniepflaum');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ':x S[Sv|8!9Whg?N%eg:=s37&_~|+7Ne-L$L%3h{C%EQf44`072WD-:J-@<t3Jo@');
define('SECURE_AUTH_KEY',  '#fr~UlQ3sdL/PP7HR6`a:;8n*{D@S@!065USQtNL;LCb0=-gt-E{=Jp-c=oORX=[');
define('LOGGED_IN_KEY',    '6o<qfYa,1s{-=IAfkj#<2{[Ya0nc2||QRmpSE3*j8w+P-d3rjn230^<P5/![f ? ');
define('NONCE_KEY',        'x^HhRHOK3? `}r) (LL1>K~ I Qif,`~Xtg8NgoYLGD?1YqPo_m~+-yT~w|r(^h6');
define('AUTH_SALT',        '%c!=Nc+(Cx|(2gmI-XMq{G,6aGvb8z|l?I!2E,C%Z|R!Ak@|>ftc?nsP+<k7.ZPz');
define('SECURE_AUTH_SALT', 'byf USpH!WZv-42{Y,~RNMx(I=2E&+N)nT}4&x(nyLAI,-Kbvybu,Y_n$|<0{muD');
define('LOGGED_IN_SALT',   'T.y=q$Xf73Z u-M~F)],w<|Uza>xm8X!-kM7QPkS@m];#t^.[iN1ht[Lf-k!-ig4');
define('NONCE_SALT',       'z-_~M@xqAntMv%j`=2)_-5[h|`fy`:C=;r%gl2@1XrSH0z~fpEGVSo-R+D]-+U/t');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
