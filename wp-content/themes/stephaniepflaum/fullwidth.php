<?php
/*
Template Name: Full Width Page
*/

/**
 * The template for displaying Full Width Page.
 *
 * @package osk
 * @since osk 1.0
 */

get_header(); ?>

<div id="primary">
  <main id="main" class="site-main fullwidth" role="main">
    <div class="wrapper">

      <?php while ( have_posts() ) : the_post(); ?>

        <?php get_template_part( 'content', 'gallery' ); ?>

      <?php endwhile; // end of the loop. ?>

    </div><!-- .wrapper -->
  </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>