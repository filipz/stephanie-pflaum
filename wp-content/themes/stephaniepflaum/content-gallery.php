<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Stephanie Pflaum
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content gallery">
		<div class="owl-carousel">

			<?php

			$images = get_field('gallery');

			if( $images ): ?>

			<?php foreach( $images as $image ): ?>
			<div class="side-image">
				<div class="content">
						<h4><?php echo $image['title']; ?></h4>
						<h1><?php echo $image['caption']; ?></h1>
						<p><?php echo $image['description']; ?></p>
				</div>
				<div class="images-wrapper">
						<a href="<?php echo $image['url']; ?>" data-imagelightbox="f"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
				</div><!-- .images-wrapper -->
			</div>
			<?php endforeach; ?>
			<?php endif; ?>

		</div><!-- .owl-carousel -->
	</div><!-- .entry-content -->
	<footer class="entry-footer">
		<?php edit_post_link( __( 'Edit', 'stephaniepflaum' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
